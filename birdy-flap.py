import pygame
import random
import time

pygame.init()

screenWidth, screenHeight = 640, 480
screenSize = (screenWidth,screenHeight)
screen = pygame.display.set_mode(screenSize)
fps = 60
clock = pygame.time.Clock()

birdImg = pygame.image.load("bird.png")
birdImgWidth = 64
birdImgHeight = 64
birdHitboxHeight = 48
birdHitboxWidth = 48
pipeTopImg = pygame.image.load("pipe-top.png")
pipeBottomImg = pygame.image.load("pipe-bottom.png")
pipeHitboxWidth = 64
pipeWidth = 64
pipeHeight = 600

white = (255,255,255)
background = white
backgroundImg = pygame.image.load('background.png')

gravity = 800
xBird = 64
yBird = screenHeight / 2
vBird = 0
flapSpeed = 300
deadFallSpeed = 600



#pipes = [[x, y, holesize]]
pipes = [[0,0,0] for i in range(5)]
pipeSpeed = 200
minPipeDist = 200
maxPipeDist = 300

def nextPipeX():
	xMax = screenWidth - minPipeDist
	for pipe in pipes:
		if pipe[0] > xMax: xMax = pipe[0]
	nextX = xMax + random.randint(minPipeDist, maxPipeDist)
	return nextX


alive = True
running = True
while running:
	#get deltaT
	deltatime = 1/fps

	# check if closed & get keys
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False
		if alive and event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
			vBird = flapSpeed

	# move pipes
	if alive or yBird < screenHeight - birdHitboxHeight/2:
		for pipe in pipes:
			pipe[0] -= pipeSpeed * deltatime
			if pipe[0] < -pipeWidth:
				pipe[0] = nextPipeX()
				pipe[1] = random.randint(screenHeight/4, screenHeight*3/4)
				pipe[2] = random.randint(128, screenHeight/2)

	# move bird
	if not alive:
		if vBird > 0: vBird = 0
		yBird += deadFallSpeed * deltatime
	vBird -= gravity * deltatime
	yBird -= vBird * deltatime
	
	#collisions
	birdBottomBound = yBird + birdHitboxHeight/2
	birdTopBound = yBird - birdHitboxHeight/2
	birdRightBound = xBird + birdHitboxWidth/2
	birdLeftBound = xBird - birdHitboxWidth/2
	
	# collide with floor
	if birdBottomBound > screenHeight: yBird = screenHeight - birdHitboxHeight/2
	
	# collide with pipes
	for pipe in pipes:
		pipeLeftBound  = pipe[0] - pipeHitboxWidth/2
		pipeRightBound = pipe[0] + pipeHitboxWidth/2
		holeTopBound    = pipe[1] - pipe[2]/2
		holeBottomBound = pipe[1] + pipe[2]/2
		if birdRightBound > pipeLeftBound and birdLeftBound < pipeRightBound: # y-axis collision
			if birdTopBound < holeTopBound or birdBottomBound > holeBottomBound: alive = False

	# --- render ---
	# background
	screen.blit(backgroundImg, (0,0))
	# pipes
	for pipe in pipes:
		paintx = pipe[0]-pipeWidth/2
		# top pipe
		screen.blit(pipeTopImg, (paintx, pipe[1]-pipeHeight-pipe[2]/2))
		# bottom pipe
		screen.blit(pipeBottomImg, (paintx, pipe[1]+pipe[2]/2))
	# bird
	screen.blit(birdImg, (xBird - birdImgWidth/2, yBird - birdImgHeight/2))
	# output
	pygame.display.flip()
	
	# wait
	clock.tick(fps)

pygame.quit()
